if(BUILD_DOC)
  find_package(Doxygen)
  if (DOXYGEN_FOUND)

    # Copy over Doxyfile with replacements
    
    SET(DOXYGEN_IN ${CMAKE_CURRENT_SOURCE_DIR}/docs/Doxyfile.in)
    SET(DOXYGEN_OUT ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile)
    configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)

    # Copy over stylesheet
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/external/qtify/qtify.css
      ${CMAKE_CURRENT_BINARY_DIR}/qtify.css COPYONLY)


    add_custom_target(doc ALL
      COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Building Doxygen documentation"
      VERBATIM )
  else (DOXYGEN_FOUND)
    message("No doxygen binary found on the system.")
    SET(${BUILD_DOC} OFF)
  endif ()
endif()
