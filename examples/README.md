This folder should be reserved for examples that instruct the user in how to use the public interface.

To set it up, copy over the basic 'CMakeLists.txt' structure from the apps folder, and add "add_subdirectory(examples)" to the root CMakeLists.txt file.