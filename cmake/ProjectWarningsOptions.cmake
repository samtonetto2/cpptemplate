# This adds two interface-only libraries that can be linked to
# any CMake target. project_warnings adds a bunch of warning
# compilation flags. project_options adds a bunch of other options.

add_library(project_warnings INTERFACE)
add_library(project_options INTERFACE)

target_compile_features(project_options INTERFACE cxx_std_17)

if (ENABLE_ASAN)
  target_compile_options(project_options INTERFACE -fsanitize=address)
  target_link_libraries(project_options INTERFACE -fsanitize=address)
endif()

target_compile_options(project_warnings
  INTERFACE
  -Wall
  -Wextra
  -Wshadow
  -Wnon-virtual-dtor
  -Wold-style-cast
  -Wcast-align
  -Wunused
  -Woverloaded-virtual
  -Wnull-dereference
  -Wdouble-promotion
  -Wformat=2)

# Excluded: -Wpedantic, -Wsign-conversion, -Wconversion


# Some GCC-specific warnings. These flags are added only if the compiler is GCC.
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  target_compile_options(project_warnings
    INTERFACE
    -Wmisleading-indentation
    -Wduplicated-cond
    -Wlogical-op
    -Wuseless-cast
    )
  target_link_libraries(project_options INTERFACE stdc++fs)
endif()

# Colored output
if (${FORCE_COLORED_OUTPUT})
  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    target_compile_options (project_options INTERFACE -fdiagnostics-color=always)
  elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    target_compile_options (project_options INTERFACE -fcolor-diagnostics)
  endif ()
endif ()
