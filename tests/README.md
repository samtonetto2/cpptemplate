This folder is for unit tests. The project is set up to use doctest, which is a light-weight and efficient alternative to Catch2, a popular C++ unit-testing testing framework.

Unit tests written with doctest don't require a main() function. Instead, main() is provided by including the following two lines at the top of each test file:

#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

See test_example.cpp, for example.