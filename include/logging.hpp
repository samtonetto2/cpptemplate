#pragma once

#include <chrono>
#include <ostream>

namespace Meta
{

/// This is a simple logger that logs program metadata to a timestamped output
/// file.
///
/// It records:
/// - project name
/// - project version
/// - git hash (if it exists)
/// - program start time
/// - program end time
/// - program duration
class Logger
{
  public:
    Logger();
    Logger(const std::string& log_path);

    /// Log metadata to output file.
    void log();

  private:
    using time_point = std::chrono::time_point<std::chrono::system_clock>;

    time_point start;
    time_point end;
    bool end_set = false;

    std::string log_path;
    bool log_path_set = false;

    void write_project_name(std::ostream& os) const;
    void write_version(std::ostream& os) const;
    void write_hash(std::ostream& os) const;

    void write_start_time(std::ostream& os) const;
    void write_end_time(std::ostream& os);
    void write_duration(std::ostream& os);

    std::string get_start_time() const;
};

} // namespace Meta
