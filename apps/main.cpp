#include <iostream>

#include "logging.hpp"

int main()
{
    Meta::Logger logger;

    logger.log();

    return 0;
}
