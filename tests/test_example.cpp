#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"

TEST_CASE("sample test")
{
  int x = 1;
  
  SUBCASE("is x equal to 1?")
    {
      CHECK(x == 1);
    }

  SUBCASE("is x equal to 0?")
    {
      CHECK(x == 0);
    }
}
