This folder is just for additional cmake scripts. Used to modularise the root CMakeLists.txt so it doesn't become too bloated.

Both users and developers can mostly ignore this folder, as it only relates to CMake setup.