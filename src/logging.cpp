#include <chrono>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

#include "config.hpp"
#include "logging.hpp"

namespace Meta
{

Logger::Logger(const std::string& log_path)
    : start{std::chrono::system_clock::now()}, log_path{log_path}, log_path_set{
                                                                       true}
{
}

Logger::Logger() : start{std::chrono::system_clock::now()} {}

void Logger::log()
{
    if (!log_path_set)
    {
        namespace fs = std::filesystem;

        const fs::path p("./" + get_start_time());

        try
        {
            if (fs::exists(p))
            {
                throw "Directory already exists.";
            }
            fs::create_directory(p);
            log_path = std::string(p) + "/program_info";
        }
        catch (char* str)
        {
            std::cout << "Exception: " << str << std::endl;
        }
    }

    std::ofstream output_file(log_path);
    write_project_name(output_file);
    write_version(output_file);
    write_hash(output_file);
    write_start_time(output_file);
    write_end_time(output_file);
    write_duration(output_file);
    output_file.close();
}

void Logger::write_start_time(std::ostream& os) const
{
    auto start_time_t = std::chrono::system_clock::to_time_t(start);
    os << "start_time\t"
       << std::put_time(std::localtime(&start_time_t), "%c %Z") << '\n';
}

void Logger::write_end_time(std::ostream& os)
{
    end = std::chrono::system_clock::now();
    end_set = true;

    auto end_time_t = std::chrono::system_clock::to_time_t(end);
    os << "end_time\t" << std::put_time(std::localtime(&end_time_t), "%c %Z")
       << '\n';
}

void Logger::write_duration(std::ostream& os)
{
    if (!end_set)
    {
        end = std::chrono::system_clock::now();
        end_set = true;
    }
    auto elapsed_ms =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
            .count();
    os << "elapsed_ms\t" << elapsed_ms << "ms\n";
}

void Logger::write_project_name(std::ostream& os) const
{
    os << "project_name\t" << PROJECT_NAME << '\n';
}

void Logger::write_hash(std::ostream& os) const
{
    os << "git_hash\t" << GIT_SHA1 << '\n';
}

void Logger::write_version(std::ostream& os) const
{
    os << "version\t\t" << PROJECT_VERSION_MAJOR << "." << PROJECT_VERSION_MINOR
       << "." << PROJECT_VERSION_PATCH << '\n';
}

std::string Logger::get_start_time() const
{
    auto start_time_t = std::chrono::system_clock::to_time_t(start);
    std::stringstream ss;
    ss << std::put_time(std::localtime(&start_time_t), "%c %Z");
    return ss.str();
}

} // namespace Meta
