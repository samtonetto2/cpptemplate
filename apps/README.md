The apps folder should be for final executables.

Include "logging.hpp" in apps to automatically log some basic thing about the program such as its time-taken, git hash, version etc of the running program. This can be important for making sure you know which version of a program the results were obtained with.